import 'package:circle_wheel_scroll/circle_wheel_scroll_view.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_circular_text/circular_text.dart';
import 'package:profile_skeleton/wheel_page.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      home: MyProfilePage(title: 'Profile'),
    );
  }
}

class MyProfilePage extends StatelessWidget {
  final String title;

  MyProfilePage({this.title});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        title: Center(
            child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Icon(Icons.help),
            Text(
              title,
              style: TextStyle(
                  color: Colors.black,
                  fontSize: 14,
                  fontWeight: FontWeight.w300),
            ),
            Icon(Icons.notifications_none)
          ],
        )),
        elevation: 0,
        iconTheme: IconThemeData(
          color: Colors.black,
          opacity: 10,
        ),
      ),
      bottomSheet: Container(
        height: 50,
        width: MediaQuery.of(context).size.width,
        color: Colors.white,
        child: Center(
            child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            Icon(
              Icons.home,
              color: Colors.black,
            ),
            Icon(
              Icons.list,
              color: Colors.black,
            ),
            Icon(
              Icons.person,
              color: Colors.purple,
            )
          ],
        )),
      ),
      body: Container(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          color: Color(0xffededed),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Container(
                height: 420,
                decoration: BoxDecoration(
                  shape: BoxShape.rectangle,
                  boxShadow: [
                    new BoxShadow(
                      color: Colors.grey,
                      offset: Offset(0, -70),
                      blurRadius: 100,
                    )
                  ],
                ),
                child: ClipPath(
                  clipper: ContainerClipper(),
                  child: new Container(
                    decoration: new BoxDecoration(
                      color: Colors.white,
                    ),
                    child: Stack(
                      children: <Widget>[
                        Column(
                          children: <Widget>[
                            Center(
                              child: Column(
                                children: <Widget>[
                                  Padding(
                                      padding: const EdgeInsets.all(12.0),
                                      child: Row(
                                        mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                        children: <Widget>[
                                          Container(
                                            height: 15,
                                            width:
                                            MediaQuery.of(context).size.width *
                                                0.85,
                                            decoration: BoxDecoration(
                                              borderRadius:
                                              BorderRadius.circular(10),
                                              color: Color(0xffededed),
                                            ),
                                          ),
                                          Text(
                                            '0%',
                                            style: TextStyle(
                                                color: Colors.black,
                                                fontSize: 12,
                                                fontWeight: FontWeight.w300),
                                          )
                                        ],
                                      )),
                                  Padding(
                                    padding: const EdgeInsets.only(bottom: 25.0),
                                    child: Text(
                                      'Hi, Karim Sianipar Finish your bio progress !!',
                                      style: TextStyle(
                                          color: Colors.black,
                                          fontSize: 14,
                                          fontWeight: FontWeight.w500),
                                    ),
                                  ),
                                  Container(
                                    height: 150,
                                    child: ClipRRect(
                                      borderRadius: BorderRadius.circular(100),
                                      child: Image.asset(
                                        'assets/images/no-pp.png',
                                        fit: BoxFit.cover,
                                      ),
                                    ),
                                  ),
                                  Padding(
                                    padding:
                                    const EdgeInsets.symmetric(vertical: 10.0),
                                    child: Text(
                                      'Add Your Picture',
                                      style: TextStyle(
                                          color: Colors.black,
                                          fontSize: 10,
                                          fontWeight: FontWeight.w300),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                        Padding(
                          padding: const EdgeInsets.only(top:200.0),
                          child: SafeArea(
                            child: Container(
                              height: 200,
                              child: CircleListScrollView(
                                physics: CircleFixedExtentScrollPhysics(),
                                axis: Axis.horizontal,
                                itemExtent: 60,
                                children: List.generate(20, _buildItem),
                                radius: MediaQuery.of(context).size.width * 0.5,
                              ),
                            ),
                          ),
                        ),
                      ],
                    )
                  ),
                ),
              ),
//              Text(
//                'Show My QR',
//                style: TextStyle(
//                    color: Colors.purple,
//                    fontSize: 12,
//                    fontWeight: FontWeight.w600),
//              ),
              RaisedButton(
                child: Text('to Wheel'),
                onPressed: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => WheelPage()));
                },
              ),
            ],
          )),
    );
  }

  Widget _buildItem(int i) {
    return Center(
      child: ClipRRect(
        borderRadius: BorderRadius.circular(40),
        child: Container(
          height: 40,
          width: 80,
          color: Colors.purple,
          child: Center(
            child: Text(
              'menu $i',
            ),
          ),
        ),
      ),
    );
  }
}

class ContainerClipper extends CustomClipper<Path> {
  @override
  getClip(Size size) {
    Path path = Path();
    path.lineTo(0, size.height * 0.75);
    path.quadraticBezierTo(
        size.width / 2, size.height, size.width, size.height * 0.75);
    path.lineTo(size.width, 0);
    path.close();
    return path;
  }

  @override
  bool shouldReclip(CustomClipper oldClipper) => false;
}
