import 'package:circle_wheel_scroll/circle_wheel_scroll_view.dart';
import 'package:flutter/material.dart';

class WheelPage extends StatelessWidget {
  Widget _buildItem(int i) {
    return Center(
      child: ClipRRect(
        borderRadius: BorderRadius.circular(40),
        child: Container(
          width: 80,
          padding: EdgeInsets.all(20),
          color: Colors.blue[100 * ((i % 8) + 1)],
          child: Center(
            child: Text(
              'menu $i',
            ),
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text('wheel'),
      ),
      body: SafeArea(
        child: Container(
          padding: const EdgeInsets.only(top:20.0),
          child: Container(
            height: 200,
              color: Colors.grey,
              child: Padding(
                padding: const EdgeInsets.only(top: 30.0),
                child: CircleListScrollView(
                  physics: CircleFixedExtentScrollPhysics(),
                  axis: Axis.horizontal,
                  itemExtent: 60,
                  children: List.generate(100, _buildItem),
                  radius: MediaQuery.of(context).size.width * -0.6,
                ),
              ),
            ),
        ),
        ),
    );
  }
}
